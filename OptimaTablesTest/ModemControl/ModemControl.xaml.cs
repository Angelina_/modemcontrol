﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ModemControl
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlModem : UserControl, ITableEvents
    {
        public ModemProperty ModemWindow;

        #region Events
        public event EventHandler<TableEvents> OnAddRecord = (object sender, TableEvents data) => { };
        public event EventHandler<TableEvents> OnChangeRecord = (object sender, TableEvents data) => { };
        public event EventHandler<TableEvents> OnDeleteRecord = (object sender, TableEvents data) => { };
        public event EventHandler OnClearRecords;

        // Открылось окно с PropertyGrid
        //public event EventHandler<FreqRangesProperty> OnIsWindowPropertyOpen = (object sender, FreqRangesProperty data) => { };
        #endregion

        public UserControlModem()
        {
            InitializeComponent();

            DgvModem.DataContext = new GlobalModem();

            //DependencyPropertyModem.ByteUpDownMaxNumber = 5;

        }

        private void ButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ModemWindow = new ModemProperty(((GlobalModem)DgvModem.DataContext).CollectionModem);

                PropertyIsOpen.IsRecAdd = true;

                ChangeLanguagePropertyGrid(DependencyPropertyModem.LanguageModem, ModemWindow.propertyGrid);
                SetTranslation(DependencyPropertyModem.LanguageModem);

                if (ModemWindow.ShowDialog() == true)
                {
                    // Событие добавления одной записи
                    OnAddRecord(this, new TableEvents(ModemWindow.Modem));
                }
            }
            catch { }
        }

        private void ButtonChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((ModemModel)DgvModem.SelectedItem != null)
                {
                    if (((ModemModel)DgvModem.SelectedItem).Number > 0)
                    {
                        var selected = (ModemModel)DgvModem.SelectedItem;

                        ModemWindow = new ModemProperty(((GlobalModem)DgvModem.DataContext).CollectionModem, selected.Clone());

                        PropertyIsOpen.IsRecChange = true;

                        ChangeLanguagePropertyGrid(DependencyPropertyModem.LanguageModem, ModemWindow.propertyGrid);
                        SetTranslation(DependencyPropertyModem.LanguageModem);

                        if (ModemWindow.ShowDialog() == true)
                        {
                            // Событие изменения одной записи
                            OnChangeRecord(this, new TableEvents(ModemWindow.Modem));
                        }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((ModemModel)DgvModem.SelectedItem != null)
                {
                    if (!IsSelectedRowEmpty())
                        return;

                    // Событие удаления одной записи
                    OnDeleteRecord(this, new TableEvents((ModemModel)DgvModem.SelectedItem));
                }
            }
            catch { }
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Событие удаления записей
                OnClearRecords(this, null);
            }
            catch { }
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((ModemModel)DgvModem.SelectedItem).Number > 0)
                {
                    if (((ModemModel)DgvModem.SelectedItem).IsActive)
                    {
                        ((ModemModel)DgvModem.SelectedItem).IsActive = false;
                    }
                    else
                    {
                        ((ModemModel)DgvModem.SelectedItem).IsActive = true;
                    }

                    // Событие изменения одной записи
                    OnChangeRecord(this, new TableEvents((ModemModel)DgvModem.SelectedItem));
                }
                else
                {
                    CheckBox chbIsChecked = sender as CheckBox;
                    chbIsChecked.IsChecked = false;
                }
            }
            catch { }
        }

        private void DgvModem_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            AddEmptyRows();
        }

        private void DgvModem_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((ModemModel)DgvModem.SelectedItem == null) return;

            if (((ModemModel)DgvModem.SelectedItem).Number > 0)
            {
                PropertyModem.SelectedNumber = ((ModemModel)DgvModem.SelectedItem).Number;
            }
        }
    }
}
