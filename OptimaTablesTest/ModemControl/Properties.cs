﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModemControl
{
    public class PropertyModem
    {
        public static byte BudNumber
        {
            get { return budNumber; }
            set
            {
                if (budNumber != value)
                    budNumber = value;
            }
        }
        private static byte budNumber = 1;

        public static int SelectedNumber
        {
            get { return selectedNumber; }
            set
            {
                if (selectedNumber != value)
                    selectedNumber = value;
            }
        }
        private static int selectedNumber = 0;

    }

    public class PropertyIsOpen
    {
        private static bool isRecAdd = false;
        public static bool IsRecAdd
        {
            get { return isRecAdd; }
            set
            {
                if (isRecAdd == value)
                    return;

                isRecAdd = value;
            }
        }

        private static bool isRecChange = false;
        public static bool IsRecChange
        {
            get { return isRecChange; }
            set
            {
                if (isRecChange == value)
                    return;

                isRecChange = value;
            }
        }
    }

    public class DependencyPropertyModem
    {
        private static byte byteUpDownMaxNumber = 9;
        public static byte ByteUpDownMaxNumber
        {
            get { return byteUpDownMaxNumber; }
            set
            {
                if (byteUpDownMaxNumber == value)
                    return;

                byteUpDownMaxNumber = value;
            }
        }

        private static Language languageModem;
        public static Language LanguageModem
        {
            get { return languageModem; }
            set
            {
                if (languageModem == value)
                    return;

                languageModem = value;
            }
        }
    }
}
