﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ModemControl
{
    public partial class UserControlModem : UserControl, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        #region Language

        //private static Language languageModem;
        //public static Language LanguageModem
        //{
        //    get { return languageModem; }
        //    set
        //    {
        //        if (languageModem == value)
        //            return;

        //        languageModem = value;
        //    }
        //}

        public static readonly DependencyProperty TranslationModemProperty = DependencyProperty.Register("TranslationModem", typeof(byte), typeof(UserControlModem),
                                                                       new PropertyMetadata((byte)1, new PropertyChangedCallback(TranslationModemChanged)));

        public byte TranslationModem
        {
            get { return (byte)GetValue(TranslationModemProperty); }
            set { SetValue(TranslationModemProperty, value); }
        }

        private static void TranslationModemChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UserControlModem userControlModem = (UserControlModem)d;
                userControlModem.UpdateLanguage();
            }
            catch
            { }

        }

        /// <summary>
        /// Обновить язык
        /// </summary>
        private void UpdateLanguage()
        {
            try
            {
                //LanguageModem = (Language)TranslationModem;
                //SetTranslation(LanguageModem);

                DependencyPropertyModem.LanguageModem = (Language)TranslationModem;
                SetTranslation(DependencyPropertyModem.LanguageModem);
            }
            catch { }
        }

        #endregion

        #region CountNumber

        public static readonly DependencyProperty CountNumberProperty = DependencyProperty.Register("CountNumber", typeof(byte), typeof(UserControlModem),
                                                                       new PropertyMetadata((byte)1, new PropertyChangedCallback(CountNumberChanged)));

        public byte CountNumber
        {
            get { return (byte)GetValue(CountNumberProperty); }
            set { SetValue(CountNumberProperty, value); }
        }

        private static void CountNumberChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                UserControlModem userControlModem = (UserControlModem)d;
                userControlModem.UpdateCountNumber();
            }
            catch
            { }

        }

        /// <summary>
        /// Обновить язык
        /// </summary>
        private void UpdateCountNumber()
        {
            try
            {
                DependencyPropertyModem.ByteUpDownMaxNumber = CountNumber;
            }
            catch { }
        }

        #endregion
    }
}
