﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModemControl
{
    public static class ValuesCorrect
    {
        /// <summary>
        /// Проверка возможноси добавления номера 
        /// </summary>
        /// <param name="collectionTemp"> локальные пункты в таблице </param>
        /// <param name="ModemWindow">добавляемый пункт</param>
        /// <returns> true - можно добавить, false - нельзя (пункт с такм номером уже есть) </returns>
        public static bool IsAddNumber(ObservableCollection<ModemModel> collectionTemp, ModemModel ModemWindow)
        {
            bool bAdd = true;

            int ind = collectionTemp.ToList().FindIndex(x => x.Number == ModemWindow.Number);
            if (ind != -1)
            {
                MessageBox.Show(SMessages.mesNumber + ModemWindow.Number + SMessages.mesAlreadyExists, SMessages.mesMessage, MessageBoxButtons.OK, MessageBoxIcon.Information);

                bAdd = false;
            }

            return bAdd;
        }
    }
}
