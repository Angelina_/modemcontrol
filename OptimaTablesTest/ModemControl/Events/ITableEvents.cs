﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModemControl
{
    public interface ITableEvents
    {
        event EventHandler<TableEvents> OnAddRecord;
        event EventHandler<TableEvents> OnChangeRecord;
        event EventHandler<TableEvents> OnDeleteRecord;
        event EventHandler OnClearRecords;
    }
}
