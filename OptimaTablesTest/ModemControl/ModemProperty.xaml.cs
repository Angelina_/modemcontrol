﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace ModemControl
{
    /// <summary>
    /// Логика взаимодействия для ModemProperty.xaml
    /// </summary>
    public partial class ModemProperty : Window
    {
        private ObservableCollection<ModemModel> collectionTemp;
        public ModemModel Modem { get; private set; }

        public ModemProperty(ObservableCollection<ModemModel> collectionModem)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionModem;
                Modem = new ModemModel();
                propertyGrid.SelectedObject = Modem;
                propertyGrid.Properties[nameof(ModemModel.Number)].IsReadOnly = false;

                Title = SMeaning.meaningAddRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/AddRec.ico", UriKind.Absolute));

                InitProperty();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        public ModemProperty(ObservableCollection<ModemModel> collectionModem, ModemModel modemModel)
        {
            try
            {
                InitializeComponent();

                InitEditors();

                collectionTemp = collectionModem;
                Modem = modemModel;
                propertyGrid.SelectedObject = Modem;

                PropertyModem.BudNumber = Modem.Number;

                propertyGrid.Properties[nameof(ModemModel.Number)].IsReadOnly = true;

                Title = SMeaning.meaningChangeRecord;
                Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Resources/ChangeRec.ico", UriKind.Absolute));

                InitProperty();
            }
            catch { }
        }

        public ModemProperty()
        {
            InitializeComponent();

            InitEditors();

            InitProperty();

        }

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IsAddClick((ModemModel)propertyGrid.SelectedObject) != null)
                {
                    DialogResult = true;
                }
            }
            catch { }
        }

        public ModemModel IsAddClick(ModemModel ModemWindow)
        {
            if(PropertyIsOpen.IsRecChange) return ModemWindow;

            if (ValuesCorrect.IsAddNumber(collectionTemp, ModemWindow))
            {
                return ModemWindow;
            }

            return null;
        }

        private void InitEditors()
        {
            propertyGrid.Editors.Add(new ModemNumberEditor(nameof(Modem.Number), typeof(ModemModel)));
            propertyGrid.Editors.Add(new ModemIPAddressEditor(nameof(Modem.IPAddress), typeof(ModemModel)));
            propertyGrid.Editors.Add(new ModemNoteEditor(nameof(Modem.Note), typeof(ModemModel)));
            propertyGrid.Editors.Add(new ModemLatitudeEditor(nameof(Modem.Latitude), typeof(ModemModel)));
            propertyGrid.Editors.Add(new ModemLongitudeEditor(nameof(Modem.Longitude), typeof(ModemModel)));
        }

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false) { continue; }

                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { System.Windows.MessageBox.Show(ex.Message); }
            }
        }

        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void gridProperty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Enter)
            {
                if (IsAddClick((ModemModel)propertyGrid.SelectedObject) != null)
                {
                    DialogResult = true;
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            PropertyIsOpen.IsRecAdd = false;
            PropertyIsOpen.IsRecChange = false;
        }
    }
}
