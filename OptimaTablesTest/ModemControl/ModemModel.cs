﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.WpfPropertyGrid;

namespace ModemControl
{
    [DataContract]
    [CategoryOrder("Общие", 1)]
    [CategoryOrder("Coordinates", 2)]
    [CategoryOrder("Прочее", 3)]
    public class ModemModel 
    {
        [Browsable(false)]
        public int Id { get; set; }

        [Browsable(false)]
        public bool IsActive { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Number))]
        public byte Number { get; set; }

        [Browsable(false)]
        public GPS GPS { get; set; }

        [Browsable(false)]
        public GLONASS GLONASS { get; set; }

        [Browsable(false)]
        public Radio RadioF { get; set; }

        [Browsable(false)]
        public Led GSM { get; set; }

        [Browsable(false)]
        public Led Radio { get; set; }

        [Browsable(false)]
        public Led RM { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(IPAddress))]
        public string IPAddress { get; set; }

        [DataMember]
        [Category("Coordinates")]
        [DisplayName(nameof(Latitude))]
        public double Latitude { get; set; }

        [DataMember]
        [Category("Coordinates")]
        [DisplayName(nameof(Longitude))]
        public double Longitude { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public String Note { get; set; } // примечание

        public ModemModel Clone()
        {
            return new ModemModel
            {
                Id = Id,
                IsActive = IsActive,
                Number = Number,
                //RadioF = new Radio
                //{
                //    F1 = RadioF.F1,
                //    F2 = RadioF.F2
                //},
                //GPS = new GPS
                //{
                //    L1 = GPS.L1,
                //    L2 = GPS.L2
                //},
                //GLONASS = new GLONASS
                //{
                //    L1 = GLONASS.L1,
                //    L2 = GLONASS.L2
                //},
                GSM = GSM,
                Radio = Radio,
                RM = RM,
                IPAddress = IPAddress,
                Latitude = Latitude,
                Longitude = Longitude,
                Note = Note
            };
        }
    }

    public class GPS
    {
        public Led L1 { get; set; } = Led.Empty;
        public Led L2 { get; set; } = Led.Empty;
    }
    public class GLONASS
    {
        public Led L1 { get; set; } = Led.Empty;
        public Led L2 { get; set; } = Led.Empty;
    }
    public class Radio
    {
        public Led F1 { get; set; } = Led.Empty;
        public Led F2 { get; set; } = Led.Empty;
    }
}
