﻿using System;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;

namespace ModemControl
{
    public class ModemNumberEditor : PropertyEditor
    {
        public ModemNumberEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/ModemControl;component/Editors/PropertyGridEditorModem.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["ModemNumberEditorKey"];
        }
    }

    public class ModemIPAddressEditor : PropertyEditor
    {
        public ModemIPAddressEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/ModemControl;component/Editors/PropertyGridEditorModem.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["ModemIPAddressEditorKey"];
        }
    }

    public class ModemNoteEditor : PropertyEditor
    {
        public ModemNoteEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/ModemControl;component/Editors/PropertyGridEditorModem.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["ModemNoteEditorKey"];
        }
    }

    public class ModemLatitudeEditor : PropertyEditor
    {
        public ModemLatitudeEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/ModemControl;component/Editors/PropertyGridEditorModem.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["ModemLatitudeEditorKey"];
        }
    }

    public class ModemLongitudeEditor : PropertyEditor
    {
        public ModemLongitudeEditor(string PropertyName, Type DeclaringType)
        {
            this.PropertyName = PropertyName;
            this.DeclaringType = DeclaringType;
            var resource = new ResourceDictionary
            {
                Source = new Uri("/ModemControl;component/Editors/PropertyGridEditorModem.xaml",
                        UriKind.RelativeOrAbsolute)
            };
            InlineTemplate = resource["ModemLongitudeEditorKey"];
        }
    }
}
