﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ModemControl
{
    public partial class UserControlModem : UserControl
    {
        private List<ModemModel> listModemModel = new List<ModemModel> { };
        public List<ModemModel> ListModemModel
        {
            get { return listModemModel; }
            set
            {
                //if (listModemModel != null && listModemModel.Equals(value)) return;
                listModemModel = value;
                UpdateModem();
            }
        }

        /// <summary>
        /// Обновить контрол
        /// </summary>
        private void UpdateModem()
        {
            try
            {
                if (ListModemModel == null)
                    return;

                ((GlobalModem)DgvModem.DataContext).CollectionModem.Clear();

                if (ListModemModel.Count == 0)
                {
                    AddEmptyRows();
                    return;
                }

                for (int i = 0; i < ListModemModel.Count; i++)
                {
                    ((GlobalModem)DgvModem.DataContext).CollectionModem.Add(ListModemModel[i]);
                }

                AddEmptyRows();

                int ind = ((GlobalModem)DgvModem.DataContext).CollectionModem.ToList().FindIndex(x => x.Number == PropertyModem.SelectedNumber);
                if (ind != -1)
                {
                    DgvModem.SelectedIndex = ind;
                }
                else
                {
                    DgvModem.SelectedIndex = 0;
                }
            }
            catch { }
        }


        /// <summary>
        /// Добавить в таблицу пустые строки
        /// </summary>
        private void AddEmptyRows()
        {
            try
            {
                int сountRowsAll = DgvModem.Items.Count; // количество всех строк в таблице
                double hs = 23; // высота строки
                double ah = DgvModem.ActualHeight; // визуализированная высота dataGrid
                double chh = DgvModem.ColumnHeaderHeight; // высота заголовка

                int countRows = Convert.ToInt32(Math.Floor(Convert.ToDouble(ah) - Convert.ToDouble(chh)) / hs); // количество строк, которое помещается в область видимости = (высота таблицы - заголовок таблицы) / высота строки

                int count = сountRowsAll - countRows; // сколько пустых строк нужно удалить
                int index = -1;
                for (int i = 0; i < count; i++)
                {
                    // Удалить пустые строки в dgv
                    index = ((GlobalModem)DgvModem.DataContext).CollectionModem.ToList().FindIndex(x => x.Id < 0);
                    if (index != -1)
                    {
                        ((GlobalModem)DgvModem.DataContext).CollectionModem.RemoveAt(index);
                    }
                }

                List<ModemModel> list = new List<ModemModel>();
                for (int i = 0; i < countRows - сountRowsAll; i++)
                {
                    ModemModel strModem = new ModemModel
                    {
                        Id = -2,
                        IsActive = false,
                        Number = 255,
                        RadioF = new Radio
                        {
                            F1 = Led.Empty,
                            F2 = Led.Empty
                        },
                        GPS = new GPS
                        {
                            L1 = Led.Empty,
                            L2 = Led.Empty
                        },
                        GLONASS = new GLONASS
                        {
                            L1 = Led.Empty,
                            L2 = Led.Empty
                        },
                        GSM = Led.Empty,
                        Radio = Led.Empty,
                        RM = Led.Empty,
                        IPAddress = string.Empty,
                        Latitude = -500,
                        Longitude = -500,
                        Note = string.Empty
                    };

                    list.Add(strModem);
                }

                for (int i = 0; i < list.Count; i++)
                {
                    ((GlobalModem)DgvModem.DataContext).CollectionModem.Add(list[i]);
                }
            }
            catch { }
        }

        private bool IsSelectedRowEmpty()
        {
            try
            {
                if (((ModemModel)DgvModem.SelectedItem).Id == -2)
                    return false;
            }
            catch { }

            return true;
        }

    }
}
