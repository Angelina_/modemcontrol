﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Xml;

namespace ModemControl
{
    public partial class UserControlModem : UserControl
    {
        public void ChangeLanguagePropertyGrid(ModemControl.Language language, System.Windows.Controls.WpfPropertyGrid.PropertyGrid propertyGrid)
        {
            foreach (var prop in propertyGrid.Properties)
            {
                SetNamePropery(prop.Name, propertyGrid);
            }

            foreach (var category in propertyGrid.Categories)
            {
                SetNameCategory(category.Name, propertyGrid);
            }
        }

        private void SetNamePropery(string nameProperty, System.Windows.Controls.WpfPropertyGrid.PropertyGrid propertyGrid)
        {
            try
            {
                propertyGrid.Properties[nameProperty].DisplayName = TranslateDic[nameProperty];
            }
            catch (Exception) { }
        }

        private void SetNameCategory(string nameCategory, System.Windows.Controls.WpfPropertyGrid.PropertyGrid propertyGrid)
        {
            try
            {
                propertyGrid.Categories.First(t => t.Name == nameCategory).HeaderCategoryName = TranslateDic[nameCategory];
            }
            catch (Exception) { }
        }

        static Dictionary<string, string> TranslateDic;
        private void LoadDictionary(Language language)
        {
            XmlDocument xDoc = new XmlDocument();

            var translation = Properties.Resources.TranslatorModem;
            xDoc.LoadXml(translation);

            TranslateDic = new Dictionary<string, string>();

            // получим корневой элемент
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr != null)
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == language.ToString())
                            {
                                if (!TranslateDic.ContainsKey(attr.Value))
                                    TranslateDic.Add(attr.Value, childnode.InnerText);
                            }
                        }
                    }
                }
            }

            if (TranslateDic.Count > 0 && TranslateDic != null)
            {
                FunctionsTranslate.RenameMeaning(TranslateDic);
                FunctionsTranslate.RenameMessages(TranslateDic);
            }
        }

        private void LoadTranslatorTable(Language language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case ModemControl.Language.RU:
                        dict.Source = new Uri("/ModemControl;component/Translation/TranslatorModem.RU.xaml",
                                      UriKind.Relative);
                        break;
                    case ModemControl.Language.EN:
                        dict.Source = new Uri("/ModemControl;component/Translation/TranslatorModem.EN.xaml",
                                           UriKind.Relative);
                        break;
                    case ModemControl.Language.AZ:
                        dict.Source = new Uri("/ModemControl;component/Translation/TranslatorModem.AZ.xaml",
                                           UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/ModemControl;component/Translation/TranslatorModem.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            { }
        }

        public void SetTranslation(Language language)
        {
            LoadDictionary(language);
            LoadTranslatorTable(language);
        }
    }
}
