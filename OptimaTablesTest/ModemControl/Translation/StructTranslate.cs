﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModemControl
{
    #region Значения
    public struct SMeaning
    {
        public static string meaningAddRecord;
        public static string meaningChangeRecord;

        public static void InitSMeaning()
        {
            meaningAddRecord = "Добавить запись";
            meaningChangeRecord = "Изменить запись";
        }
    }
    #endregion

    #region Сообщения
    public struct SMessages
    {
        public static string mesMessage;
        public static string mesNumber;
        public static string mesAlreadyExists;


        public static void InitSMessages()
        {
            mesMessage = "Сообщение!";
            mesNumber = "Номер ";
            mesAlreadyExists = " уже существует!";
        }
    }
    #endregion
}
