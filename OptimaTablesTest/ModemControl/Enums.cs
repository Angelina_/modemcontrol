﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModemControl
{
    public enum Led : byte
    {
        Empty = 0,
        Green = 1,
        Red = 2,
        Gray = 3
    }
    public enum Language : byte
    {
        RU = 0,
        EN = 1,
        AZ = 2
    }
}
