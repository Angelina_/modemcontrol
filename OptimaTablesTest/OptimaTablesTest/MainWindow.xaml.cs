﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OptimaTablesTest
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<ModemControl.ModemModel> listModem = new List<ModemControl.ModemModel>();
        
        public MainWindow()
        {
            InitializeComponent();

            DataContext = this;

            GlobalProperties = new OptimaSettingsProperties.PropertiesPropGrid();
            LocalProperties = new OptimaSettingsProperties.PropertiesPropGrid();

            //ucModem.SetTranslation((ModemControl.Language)ucProperty.Local.Common.Language);
            LocalProperties.Local.Common.Language = ucProperty.Local.Common.Language;

           


        }

        private void ucModem_OnAddRecord(object sender, ModemControl.TableEvents e)
        {
            listModem.Add(e.Record);
            ucModem.ListModemModel = listModem;
        }

        private void ucModem_OnChangeRecord(object sender, ModemControl.TableEvents e)
        {
            int ind = listModem.FindIndex(x => x.Number == e.Record.Number);
            if (ind != -1)
            {
                listModem[ind] = e.Record;
            }
            ucModem.ListModemModel = listModem;
        }

        private void ucModem_OnDeleteRecord(object sender, ModemControl.TableEvents e)
        {
            int ind = listModem.FindIndex(x => x.Number == e.Record.Number);
            if (ind != -1)
            {
                listModem.RemoveAt(ind);
            }
            ucModem.ListModemModel = listModem;
        }

        private void ucModem_OnClearRecords(object sender, EventArgs e)
        {
            listModem = new List<ModemControl.ModemModel>();
            ucModem.ListModemModel = listModem;
        }

        private void ucProperty_OnLanguageChanged(object sender, OptimaSettingsProperties.Model.Languages e)
        {
             LocalProperties.Local.Common.Language = ucProperty.Local.Common.Language;

           
        }

        private void ucProperty_OnLocalPropertiesChanged(object sender, OptimaSettingsProperties.Model.PropertiesOfOPY e)
        {
            LocalProperties.Local = ucProperty.Local;
        }
    }
}
