﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OptimaTablesTest
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion

        private OptimaSettingsProperties.PropertiesPropGrid _localProperties;
        public OptimaSettingsProperties.PropertiesPropGrid LocalProperties
        {
            get { return _localProperties; }
            set
            {
                if (_localProperties != value)
                {
                    _localProperties = value;
                    OnPropertyChanged();
                }
            }
        }

        private OptimaSettingsProperties.PropertiesPropGrid _globalProperties;
        public OptimaSettingsProperties.PropertiesPropGrid GlobalProperties
        {
            get { return _globalProperties; }
            set
            {
                if (_globalProperties != value)
                {
                    _globalProperties = value;
                    OnPropertyChanged();

                }
            }
        }
    }
}
